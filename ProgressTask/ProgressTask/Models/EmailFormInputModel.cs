﻿using System.ComponentModel.DataAnnotations;

namespace ProgressTask.Web.Models
{
    public class EmailFormInputModel
    {
        [Required]
        [EmailAddress]
        [MinLength(2, ErrorMessage = "Email must be atleast 3 symbols")]
        public string Email { get; set; }
    }
}
