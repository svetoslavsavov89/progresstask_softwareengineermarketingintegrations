﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

$(document).ready(function () {
    $("#Save").click(function () {
        var emailForm = new Object();
        emailForm.Email = $("#email").val();
        if (emailForm.Email == "" || emailForm.Email == undefined || emailForm.Email.length < 3) {
            alert("Email must be atleast 3 symbols")
            return false;
        }
        $.ajax({
            url: '/Home/Send',
            type: 'POST',
            dataType: "json",
            data: {
                __RequestVerificationToken: gettoken(),
                emailForm
            },
            contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
            beforeSend: (function () {
                $("#email").css({ "border-color": '' });
                $("#icon").css({ "color": '' });
                $("#input-group").css({ "border-color": '' });
                $("#wait").css("display", "block");
                $('.row').css({ "opacity": "0.3" });
                $('.form-control').attr('disabled', true);
                $("#Save").attr('disabled', true);
            }),
            complete: function (xhr, textStatus, errorThrown) {
                $("#wait").css("display", "none");
                $('.row').css({ "opacity": "" });
                $("#Save").attr('disabled', false);
                $('.form-control').attr('disabled', false);
                if (xhr.status == 200) {
                    $("#input").trigger("reset");
                    toastr.success('You successfully submited your email!');
                }
                if (xhr.status == 400) {
                    $("#email").css({ "border-color": "#dc3545" });
                    $("#icon").css({ "color": "#dc3545" });
                    $("#input-group").css({ "border-color": "#dc3545" });
                    toastr.error('Error - no Email sent! Please provide email@email');
                }
            }
        });
    });
});