﻿using Microsoft.AspNetCore.Mvc;
using ProgressTask.Services.Contracts;
using ProgressTask.Web.Models;
using System;
using System.Threading.Tasks;

namespace ProgressTask.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IEmailFormServices _emailServices;

        public HomeController(IEmailFormServices emailServices)
        {
            _emailServices = emailServices;
        }

        public IActionResult Index()
        {
            return View();
        }   

        [HttpPost]
        [ValidateAntiForgeryToken]  
        public async Task<IActionResult> Send(EmailFormInputModel emailForm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    bool result = await _emailServices.ExecuteWithRetry(emailForm.Email);

                    if (!result)
                    {
                        return StatusCode(400);
                    }
                    else
                    {
                        return StatusCode(200);
                    }
                }
                return StatusCode(400);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

    }
}
