﻿using System.Threading.Tasks;

namespace ProgressTask.Services.Contracts
{
    public interface IEmailFormServices
    {
        Task<bool> ExecuteWithRetry(string email);
    }
}
