﻿using Newtonsoft.Json;
using ProgressTask.Services.Contracts;
using RestSharp;
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ProgressTask.Services
{
    public class EmailFormServices : IEmailFormServices
    {
        public async Task<bool> ExecuteWithRetry(string email)
        {
            bool result = false;

            if (IsValidEmail(email))
            {
                XmlDocument doc = new XmlDocument();
                string fileName = "Path.xml";
                var fullname = Path.GetFullPath(fileName);
                doc.Load(fullname);

                string url = doc.DocumentElement.InnerText;

                var client = new RestClient(url);
                var request = new RestRequest(Method.POST);
                request.RequestFormat = DataFormat.Json;
                var input = JsonConvert.SerializeObject(new { email = email });
                request.AddJsonBody(input.ToLower());

                do
                {
                    IRestResponse response = await client.ExecutePostAsync(request);

                    if (response.Content == "Success")
                    {
                        result = true;
                    }
                    if (response.Content == "Error - no Email sent." || response.StatusDescription == "Bad Request")
                    {
                        break;
                    }
                }
                while (!result);

                return result;
            }
            return result;
        }
        private bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
    }
}
