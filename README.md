## ProgressTask_SoftwareEngineerMarketingIntegrations
Web application with user email form that submits the data from that form to external endpoint. 

## Technologies

* ASP.NET Core MVC
* Razor
* AJAX
* JavaScript
* HTML
* CSS
* Bootstrap

## Features
* A single page with a form with only one field - email;
* Integration with external API;
* RestSharp for Http client and requests;
* Build-in retry logic that resubmits the data until the submit is successful;
* Server-side and client-side validation;
* Validating AntiForgery token for the requests;
* Font Awesome icons;
* User-friendly toastr messages:
 	- when submitting is successful - green success message;
	- when submitted text is not in an e-mail format - red fail message;
* Alert messages:
 	- when submitted text is empty or lenght is less then 3 characters;
* Loading wheel while data is being submited;
* Endpoint Url in .xml file;

## Screenshots
Home page
![Home Page](Images/Home.jpg)
Loading Wheel
![Loading Wheel](Images/LoadingWheel.jpg)
Success
![Success](Images/Success.jpg)
Alert
![Alert](Images/Alert-lenghtErorr.jpg)
Error
![Error](Images/Error-incorrectFormat.jpg)